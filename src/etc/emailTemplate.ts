import marked from 'marked';
import format from 'date-fns/format';
import optimizeImage from '../lib/common';

export const createAuthEmail = (registered: boolean, code: string) => {
  const keywords = registered
    ? {
        type: 'email-login',
        text: 'login',
      }
    : {
        type: 'register',
        text: 'sign up',
      };

  const subject = `Donuts ${keywords.text}`;
  const body = `<a href="https://donuts.app"><img src="https://images.donuts.app/email-logo.png" style="display: block; width: 128px; margin: 0 auto;"/></a>
  <div style="max-width: 100%; width: 400px; margin: 0 auto; padding: 1rem; text-align: justify; background: #f8f9fa; border: 1px solid #dee2e6; box-sizing: border-box; border-radius: 4px; color: #868e96; margin-top: 0.5rem; box-sizing: border-box;">
    <b style="black">Good morning! </b>${keywords.text}Click the link below to continue. If you made a request by mistake, or if you did not request it, please disregard this email.
  </div>
  
  <a href="https://donuts.app/${keywords.type}?code=${code}" style="text-decoration: none; width: 400px; text-align:center; display:block; margin: 0 auto; margin-top: 1rem; background: #845ef7; padding-top: 1rem; color: white; font-size: 1.25rem; padding-bottom: 1rem; font-weight: 600; border-radius: 4px;">continue</a>
  
  <div style="text-align: center; margin-top: 1rem; color: #868e96; font-size: 0.85rem;"><div>Click the button above or open the following link: <br/> <a style="color: #b197fc;" href="https://donuts.app/${keywords.type}?code=${code}">https://donuts.app/${keywords.type}?code=${code}</a></div><br/><div>
  This link is valid for 24 hours. </div></div>`;

  return {
    subject,
    body,
  };
};

type CreateCommentEmailParams = {
  postWriter: string;
  username: string;
  userThumbnail: string | null;
  urlSlug: string;
  unsubscribeToken: string;
  postTitle: string;
  comment: string;
  commentId: string;
};

export const createCommentEmail = ({
  postWriter,
  username,
  userThumbnail,
  urlSlug,
  unsubscribeToken,
  postTitle,
  comment,
  commentId,
}: CreateCommentEmailParams) => {
  const commentHtml = marked(comment);
  const postLink = `https://donuts.app/@${postWriter}/${urlSlug}?comment_id=${commentId}`;
  const unsubscribeUrl = `https://v2.donuts.app/api/v2/common/email/unsubscribe?token=${unsubscribeToken}`;

  return `
<a href="https://donuts.app"
  ><img
    src="https://images.donuts.app/email-logo.png"
    style="display: block; width: 128px; margin: 0 auto; margin-bottom: 1rem;"
/></a>
<div style="max-width: 100%; width: 600px; margin: 0 auto;">
  <div style="font-weight: 400; margin: 0; font-size: 1.25rem; color: #868e96;">
  A new comment has been added to the post.
  </div>
  <div style="margin-top: 0.5rem;">
    <a
      href="${postLink}"
      style="color: #495057; text-decoration: none; font-weight: 600; font-size: 1.125rem;"
      >${postTitle}</a
    >
  </div>
  <div style="font-weight: 400; margin-top: 0.5rem; font-size: 1.75rem;"></div>
  <div
    style="width: 100%; height: 1px; background: #e9ecef; margin-top: 2rem; margin-bottom: 2rem;"
  ></div>
  <div style="display:-webkit-flex;display:-ms-flexbox;display:flex;">
    <div>
      <a href="https://donuts.app/@${username}">
        <img
          style="height: 64px; width: 64px; display: block; border-radius: 32px;"
          src="${optimizeImage(
            userThumbnail || 'http://img.donuts.app/default_user_thumbnail.png',
            120
          )}"
        />
      </a>
    </div>
    <div style="flex: 1; margin-left: 1.5rem; color: #495057;">
      <div style="margin-bottom: 0.5rem;">
        <a
          href="https://donuts.app/@${username}"
          style="text-decoration: none; color: #212529; font-weight: 600;"
          >${username}</a
        >
      </div>
      <div style="margin: 0; color: #495057;">
        ${commentHtml}
      </div>
      <div style="font-size: 0.875rem; color: #adb5bd; margin-top: 1.5rem">
        ${format(new Date(), 'yyyy MM dd')}
      </div>
      <a
        href="${postLink}"
        style="outline: none; border: none; background: #845ef7; color: white; padding-top: 0.5rem; padding-bottom: 0.5rem; font-size: 1rem; font-weight: 600; display: inline-block; background: #845ef7; padding-left: 1rem; padding-right: 1rem; align-items: center; margin-top: 1rem; border-radius: 4px; text-decoration: none;"
        >Reply</a
      >
    </div>
  </div>
  <div
    style="width: 100%; height: 1px; background: #e9ecef; margin-top: 4rem; margin-bottom: 1rem;"
  ></div>
  <div style="font-size: 0.875rem; color: #adb5bd; font-style: italic;">
  If you do not wish to receive comment notifications by email
    <a href="${unsubscribeUrl}" style="color: inherit">Please click the link.</a>
  </div>
</div>
<div>
  <br />
  <br />
  <br />
  donuts | contact@donuts.app
</div>
  `;
};
